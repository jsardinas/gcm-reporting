package com.exchangelodge.gcm.reporting.lambda.services;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRDataset;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRValueParameter;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.query.JRAbstractQueryExecuter;

import java.util.Map;

public class EntityQueryExecuter extends JRAbstractQueryExecuter {

    public static final String CANONICAL_LANGUAGE = "EQL";

    EntityQueryExecuter(JasperReportsContext jasperReportsContext, JRDataset dataset, Map<String, ? extends JRValueParameter> parameters) {
        super(jasperReportsContext, dataset, parameters);

        // parse query
        parseQuery();
    }

    @Override
    protected String getCanonicalQueryLanguage() {
        return CANONICAL_LANGUAGE;
    }

    @Override
    public JRDataSource createDatasource() throws JRException {
        return null;
    }

    @Override
    protected String getParameterReplacement(String parameterName) {
        return null;
    }

    @Override
    public void close() {
    }

    @Override
    public boolean cancelQuery() {
        return false;
    }
}
