package com.exchangelodge.gcm.reporting.lambda.services;

import com.steelbridgelabs.cbe.json.Json;
import com.steelbridgelabs.cbe.json.JsonArray;
import com.steelbridgelabs.cbe.json.JsonObject;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DummyDataSource implements JRDataSource {

    private static final Logger logger = LoggerFactory.getLogger(DummyDataSource.class);

    private JsonArray data;
    private int index = -1;

    public DummyDataSource() {
        data = Json.createArrayBuilder().add(Json.createObjectBuilder().add("concept", "Bank Loans")).build();
        logger.info("Dummy data source created with {} elements", data.size());
    }

    @Override
    public boolean next() throws JRException {
        logger.info("next(): Current index {} Total elements {}", index, data.size());
        return (++index < data.size());
    }

    @Override
    public Object getFieldValue(JRField jrField) throws JRException {
        logger.info("getFieldValue(): Current index {} Total elements {}", index, data.size());
        if (index < data.size()) {
            JsonObject element = data.getJsonObject(index);
            String fieldName = jrField.getName();
            logger.info("Searching for field name {} in element {}", fieldName, index);
            if (element.containsKey(fieldName))
                return element.getString(fieldName);
            // Don't have such field
        }
        return null;
    }
}
