package com.exchangelodge.gcm.reporting.lambda.services;

import net.sf.jasperreports.engine.JRDataset;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRValueParameter;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.query.AbstractQueryExecuterFactory;
import net.sf.jasperreports.engine.query.JRQueryExecuter;
import net.sf.jasperreports.engine.util.Designated;

import java.util.Map;

public class EntityQueryExecuterFactory extends AbstractQueryExecuterFactory implements Designated {

    private static final String QUERY_EXECUTER_NAME = "net.sf.jasperreports.query.executer:EQL";

    public static final String QUERY_LANGUAGE = "eql";

    @Override
    public Object[] getBuiltinParameters() {
        return new Object[0];
    }

    @Override
    public JRQueryExecuter createQueryExecuter(JasperReportsContext jasperReportsContext, JRDataset dataset, Map<String, ? extends JRValueParameter> parameters) throws JRException {
        return new EntityQueryExecuter(jasperReportsContext, dataset, parameters);
    }

    @Override
    public boolean supportsQueryParameterType(String className) {
        return true;
    }

    @Override
    public String getDesignation() {
        return QUERY_EXECUTER_NAME;
    }
}
