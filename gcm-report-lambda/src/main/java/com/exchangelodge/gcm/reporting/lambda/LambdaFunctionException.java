package com.exchangelodge.gcm.reporting.lambda;

public class LambdaFunctionException extends RuntimeException {

    private static final long serialVersionUID = -1L;

    private final int code;

    public LambdaFunctionException(String message, int code) {
        super(message);
        this.code = code;
    }

    public LambdaFunctionException(String message, int code, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
