package com.exchangelodge.gcm.reporting.lambda.services;

import com.steelbridgelabs.cbe.json.Json;
import com.steelbridgelabs.cbe.json.JsonArray;
import com.steelbridgelabs.cbe.json.JsonNumber;
import com.steelbridgelabs.cbe.json.JsonObject;
import com.steelbridgelabs.cbe.json.JsonObjectBuilder;
import com.steelbridgelabs.cbe.json.JsonString;
import com.steelbridgelabs.cbe.json.JsonValue;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRField;

import java.time.LocalDate;

public class ReportSummaryDataSource implements JRDataSource {

    private final MessagingService messagingService;
    private final String filter;
    private static final String entityName = "GcmPortfolioInvestmentDetail";
    private static final String select = "portfolioInfo/publishTicker as 'publishTicker',portfolioInfo/valuationDate as 'valuationDate',portfolioInfo/portfolioValuation/portfolio/portfolioAcronym as 'portfolioAcronym',portfolioInfo/portfolioValuation/portfolio/portfolioLegalName as 'portfolioLegalName',investment/managerName as 'investment',balanceForward,beginningNetActivity,openingBalance,endingBalance,income as 'netGainLoss',endingNetActivity,closingBalance,rateOfReturn as 'mtdRateOfReturn'"
        + ",ytdRateOfReturn,ytdContributionToReturn,adminCtr as 'mtdContributionToReturn',ytdContributionToReturn,portfolioInfo/portfolioSummary/balanceForward as 'totalBalanceForward',portfolioInfo/portfolioSummary/beginningNetActivity as 'totalBeginningNetActivity',portfolioInfo/portfolioSummary/openingBalance as 'totalOpeningBalance',portfolioInfo/portfolioSummary/netGainLoss as 'totalNetGainLoss',portfolioInfo/portfolioSummary/endingBalance as 'totalEndingBalance',portfolioInfo/portfolioSummary/endingNetActivity as 'totalEndingNetActivity'"
        + ",portfolioInfo/portfolioSummary/closingBalance as 'totalClosingBalance',portfolioInfo/portfolioSummary/rateOfReturn as 'totalMtdRateOfReturn',portfolioInfo/portfolioSummary/contributionToReturn as 'totalMtdContributionToReturn',portfolioInfo/portfolioSummary/rateOfReturnYtd as 'totalYtdRateOfReturn',portfolioInfo/portfolioSummary/contributionToReturnYtd as 'totalYtdContributionToReturn'"
        + ",portfolioInfo/investedSummary/balanceForward as 'investedBalanceForward',portfolioInfo/investedSummary/beginningNetActivity as 'investedBeginningNetActivity',portfolioInfo/investedSummary/openingBalance as 'investedOpeningBalance',portfolioInfo/investedSummary/netGainLoss as 'investedNetGainLoss',portfolioInfo/investedSummary/endingBalance as 'investedEndingBalance',portfolioInfo/investedSummary/endingNetActivity as 'investedEndingNetActivity'"
        + ",portfolioInfo/investedSummary/closingBalance as 'investedClosingBalance',portfolioInfo/investedSummary/rateOfReturn as 'investedMtdRateOfReturn',portfolioInfo/investedSummary/contributionToReturn as 'investedMtdContributionToReturn'"
        + ",portfolioInfo/investedSummary/rateOfReturnYtd as 'investedYtdRateOfReturn',portfolioInfo/investedSummary/contributionToReturnYtd as 'investedYtdContributionToReturn'";
    private final String tenantId;
    private final String authenticationToken;
    private JsonArray data = null;
    private int index;

    public ReportSummaryDataSource(MessagingService messagingService, String authenticationToken, String filter, String tenantId) {
        this.authenticationToken = authenticationToken;
        this.messagingService = messagingService;
        this.filter = filter;
        this.tenantId = tenantId;
        this.index = -1;
    }

    @Override
    public boolean next() {
        if (data == null) {
            // Run the query. Reset index.
            data = runQuery();
            if (data != null && !data.isEmpty()) {
                index = 0;
                return true;
            }
            return false;
        }
        return (++index < data.size());
    }

    @Override
    public Object getFieldValue(JRField jrField) {
        if (index < data.size()) {
            String fieldName = jrField.getName();
            JsonObject current = data.getJsonObject(index);
            if (current.containsKey(fieldName)) {
                JsonValue value = current.get(fieldName);
                switch (value.getValueType()) {
                    case STRING:
                        if ("valuationDate".equals(fieldName))
                            return LocalDate.parse(((JsonString)value).getString());
                        else
                            return ((JsonString)value).getString();
                    case NUMBER:
                        return ((JsonNumber)value).getDouble();
                    case TRUE:
                    case FALSE:
                        return current.getBoolean(jrField.getName());
                    default:
                        return null;
                }
            }
        }
        // Not present
        return null;
    }

    JsonArray runQuery() {
        String query = "select=" + select;
        if (filter != null)
            query += ("&filter=" + filter);
        query += "&orderby=investment/managerName";
        query += "&top=1500";
        JsonObjectBuilder builder = Json.createObjectBuilder().add("parameters", Json.createArrayBuilder().add(entityName)).add("query", query);
        if (tenantId != null)
            builder.add("tenant", tenantId);
        JsonObject message = builder.build();
        builder = Json.createObjectBuilder().add("Content-Type", "application/cbe-json").add("x-cbe-authorization", authenticationToken);
        if (tenantId != null)
            builder.add("x-tenant-id", tenantId);
        JsonObject header = builder.build();
        JsonObject result = messagingService.sendAndReceive(message, messagingService.getEntityRoutingKey(), header);
        return result.getJsonArray("value");
    }
}
