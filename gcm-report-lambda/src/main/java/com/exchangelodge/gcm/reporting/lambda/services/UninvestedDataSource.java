package com.exchangelodge.gcm.reporting.lambda.services;

import com.steelbridgelabs.cbe.json.Json;
import com.steelbridgelabs.cbe.json.JsonArray;
import com.steelbridgelabs.cbe.json.JsonNumber;
import com.steelbridgelabs.cbe.json.JsonObject;
import com.steelbridgelabs.cbe.json.JsonObjectBuilder;
import com.steelbridgelabs.cbe.json.JsonString;
import com.steelbridgelabs.cbe.json.JsonValue;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRField;

public class UninvestedDataSource implements JRDataSource {

    private final MessagingService messagingService;
    private final String filter;
    private static final String entityName = "GcmPortfolioInvestmentDetailUninvested";
    private static final String select = "name as 'concept',balanceForward,beginningNetActivity,openingBalance,income as 'netGainLoss',endingBalance,endingNetActivity,closingBalance,rateOfReturn as 'mtdRateOfReturn',ytdRateOfReturn,ytdContributionToReturn,adminCtr as 'mtdContributionToReturn',"
        + "ytdContributionToReturn,portfolioInfo/uninvestedSummary/balanceForward as 'totalBalanceForward',portfolioInfo/uninvestedSummary/beginningNetActivity as 'totalBeginningNetActivity',portfolioInfo/uninvestedSummary/openingBalance as 'totalOpeningBalance',"
        + "portfolioInfo/uninvestedSummary/netGainLoss as 'totalNetGainLoss',portfolioInfo/uninvestedSummary/endingBalance as 'totalEndingBalance',portfolioInfo/uninvestedSummary/endingNetActivity as 'totalEndingNetActivity',portfolioInfo/uninvestedSummary/closingBalance as 'totalClosingBalance',"
        + "portfolioInfo/uninvestedSummary/contributionToReturn as 'totalMtdContributionToReturn',portfolioInfo/uninvestedSummary/contributionToReturnYtd as 'totalYtdContributionToReturn'";
    private final String tenantId;
    private final String authenticationToken;
    private JsonArray data = null;
    private int index;

    public UninvestedDataSource(MessagingService messagingService, String authenticationToken, String filter, String tenantId) {
        this.authenticationToken = authenticationToken;
        this.messagingService = messagingService;
        this.filter = filter;
        this.tenantId = tenantId;
        this.index = -1;
    }

    @Override
    public boolean next() {
        if (data == null) {
            // Run the query. Reset index.
            data = runQuery();
            if (data != null && !data.isEmpty()) {
                index = 0;
                return true;
            }
            return false;
        }
        return (++index < data.size());
    }

    @Override
    public Object getFieldValue(JRField jrField) {
        JsonObject current = data.getJsonObject(index);
        if (current.containsKey(jrField.getName())) {
            JsonValue value = current.get(jrField.getName());
            switch (value.getValueType()) {
                case STRING:
                    return ((JsonString)value).getString();
                case NUMBER:
                    return ((JsonNumber)value).getDouble();
                case TRUE:
                case FALSE:
                    return current.getBoolean(jrField.getName());
                default:
                    return null;
            }
        }
        // Not present
        return null;
    }

    JsonArray runQuery() {
        String query = "select=" + select;
        if (filter != null)
            query += ("&filter=" + filter);
        query += "&orderby=name";
        query += "&top=1500";
        JsonObjectBuilder builder = Json.createObjectBuilder().add("parameters", Json.createArrayBuilder().add(entityName)).add("query", query);
        if (tenantId != null)
            builder.add("tenant", tenantId);
        JsonObject message = builder.build();
        builder = Json.createObjectBuilder().add("Content-Type", "application/cbe-json").add("x-cbe-authorization", authenticationToken);
        if (tenantId != null)
            builder.add("x-tenant-id", tenantId);
        JsonObject header = builder.build();
        JsonObject result = messagingService.sendAndReceive(message, messagingService.getEntityRoutingKey(), header);
        return result.getJsonArray("value");
    }
}
