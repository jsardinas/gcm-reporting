package com.exchangelodge.gcm.reporting.lambda.services;

import com.exchangelodge.gcm.reporting.lambda.LambdaFunctionException;
import com.steelbridgelabs.cbe.json.Json;
import com.steelbridgelabs.cbe.json.JsonArray;
import com.steelbridgelabs.cbe.json.JsonObject;
import com.steelbridgelabs.cbe.json.JsonObjectBuilder;
import com.steelbridgelabs.cbe.json.JsonValue;
import com.steelbridgelabs.cbe.json.JsonWriter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ColorScaleFormatting;
import org.apache.poi.ss.usermodel.ConditionalFormattingRule;
import org.apache.poi.ss.usermodel.ConditionalFormattingThreshold;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class ReportExecutionService {

    private static final String GROSVENOR_FUND = "Portfolio";

    private static final Logger logger = LoggerFactory.getLogger(ReportExecutionService.class);
    private final AtomicReference<MessagingService> messagingService = new AtomicReference<>();

    public ReportExecutionService() {
        initializeMessagingService();
    }

    public void execute(JsonObject event, String token, OutputStream output) {
        final String resource = event.getString("resource", "");
        final String reportName = resource.substring(resource.lastIndexOf("/") + 1);
        if ("report1".equals(reportName))
            runReport1(reportName, event, token, output);
        else
            runReport2(event, token, output);
    }

    JsonArray runQuery(String authenticationToken, String tenantId, String entityName, String select, String filter, String orderBy, boolean distinct) {
        String query = "select=" + select;
        if (filter != null)
            query += ("&filter=" + filter);
        if (orderBy != null)
            query += ("&orderby=" + orderBy);
        if (distinct)
            query += ("&distinct=true");
        query += "&top=3500";
        JsonObjectBuilder builder = Json.createObjectBuilder().add("parameters", Json.createArrayBuilder().add(entityName)).add("query", query);
        if (tenantId != null)
            builder.add("tenant", tenantId);
        JsonObject message = builder.build();
        builder = Json.createObjectBuilder().add("Content-Type", "application/cbe-json").add("x-cbe-authorization", authenticationToken);
        if (tenantId != null)
            builder.add("x-tenant-id", tenantId);
        JsonObject header = builder.build();
        JsonObject result = getMessagingService().sendAndReceive(message, messagingService.get().getEntityRoutingKey(), header);
        return result.getJsonArray("value");
    }

    JRDataSource createDataSource(String token, String reportName, String tenantId, String filter) {
        if ("report1".equals(reportName)) {
            return new ReportSummaryDataSource(getMessagingService(), token, filter, tenantId);
        }
        logger.error("Unknown report name [{}]", reportName);
        throw new LambdaFunctionException("Invalid Report name " + reportName, 404);
    }

    JasperReport loadReport(String reportName) {
        // load embedded resource
        URL url = ReportExecutionService.class.getClassLoader().getResource(reportName + ".jrxml");
        if (url != null) {
            try {
                // compile report
                return JasperCompileManager.compileReport(url.openStream());
            }
            catch (JRException | IOException ex) {
                // log information
                logger.error("Error compiling report", ex);
                // error compiling report
                throw new LambdaFunctionException("Error compiling report", 500, ex);
            }
        }
        throw new LambdaFunctionException("Cannot find report", 500);
    }

    private void runReport1(String reportName, JsonObject event, String token, OutputStream output) {
        try {
            JsonObject eventheaders = event.getJsonObject("headers");
            final String tenantId = eventheaders.getString("x-tenant-id", null);
            String filter = null;
            JsonObject queryParameters = event.getJsonObject("queryStringParameters");
            if (queryParameters != null && queryParameters.containsKey("pid")) {
                String pid = queryParameters.getString("pid");
                filter = String.format("portfolioInfo/id eq '%s'", pid);
            }
            // parameters
            Map<String, Object> parameters = new HashMap<>();
            // Create secondary items (subreports + data sources)
            JRDataSource secondarySource = new UninvestedDataSource(getMessagingService(), token, filter, tenantId);
            parameters.put("data_source", secondarySource);
            JasperReport subReport = loadReport("report1.uninvested");
            parameters.put("subreport", subReport);
            // Add report and data source for investors
            secondarySource = new InvestorsDataSource(getMessagingService(), token, filter, tenantId);
            parameters.put("data_source_investors", secondarySource);
            subReport = loadReport("report1.investors");
            parameters.put("subreport_investors", subReport);
            // Add report and data source for investors
            secondarySource = new ClassesDataSource(getMessagingService(), token, filter, tenantId);
            parameters.put("data_source_classes", secondarySource);
            subReport = loadReport("report1.classes");
            parameters.put("subreport_classes", subReport);
            // load report
            logger.info("Loading report {} with filter [{}]", reportName, filter == null ? "N/A" : filter);
            JasperReport report = loadReport(reportName);
            // Create Data source
            logger.info("Creating data source for report [{}] in tenant [{}]", reportName, tenantId);
            JRDataSource dataSource = createDataSource(token, reportName, tenantId, filter);
            // execute report
            logger.info("Filling report {}", reportName);
            JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, dataSource);
            // export to pdf
            logger.info("Exporting report to PDF");
            byte[] buffer = JasperExportManager.exportReportToPdf(jasperPrint);
            // headers
            JsonObject headers = new JsonObject()
                .add("Access-Control-Allow-Origin", "*")
                .add("Content-Length", buffer.length)
                .add("Content-Type", "application/pdf");
            // create response
            JsonObject response = new JsonObject()
                .add("isBase64Encoded", true)
                .add("statusCode", 200)
                .add("headers", headers)
                .add("body", buffer);
            // create writer
            logger.info("Serializing report output");
            try (JsonWriter writer = new JsonWriter(output, false)) {
                // write response
                writer.write(response);
            }
            output.flush();
        }
        catch (JRException | IOException ex) {
            // log information
            logger.error("Error executing report", ex);
            // error compiling report
            throw new LambdaFunctionException("Error executing report", 500, ex);
        }
    }

    void initializeMessagingService() {
        if (messagingService.get() == null) {
            // Get variables we need to build the messaging service
            String serverName = System.getenv("RABBITMQ_SERVER");
            int port = Integer.parseInt(System.getenv("RABBITMQ_PORT"));
            String userName = System.getenv("RABBITMQ_USERNAME");
            String password = System.getenv("RABBITMQ_PASSWORD");
            String virtualHost = System.getenv("RABBITMQ_VIRTUAL_HOST");
            String exchange = System.getenv("RABBITMQ_EXCHANGE");
            String entityRoutingKeyPrefix = System.getenv("ENTITY_PREFIX");
            MessagingService service = new MessagingService(serverName, port, virtualHost, userName, password, exchange, entityRoutingKeyPrefix);
            messagingService.set(service);
        }
    }

    MessagingService getMessagingService() {
        if (messagingService.get() == null)
            initializeMessagingService();
        return messagingService.get();
    }

    // ************************************  Report 2 (XLSX) ****************************************** //

    private void runReport2(JsonObject event, String token, OutputStream output) {
        JsonObject eventHeaders = event.getJsonObject("headers");
        final String tenantId = eventHeaders.getString("x-tenant-id", null);
        JsonObject queryParameters = event.getJsonObject("queryStringParameters");
        String pid = null;
        if (queryParameters != null && queryParameters.containsKey("pid"))
            pid = queryParameters.getString("pid");
        // Report2 should prepare an excel file.
        Workbook workbook = new XSSFWorkbook();
        workbook.createSheet(GROSVENOR_FUND);
        // Fill first page with portfolio data
        Map<String, Sheet> investmentSheets = new HashMap<>();
        fillFundPage(tenantId, pid, token, workbook);
        fillInvestmentsPages(tenantId, pid, token, workbook, investmentSheets);
        // Remove pages that have no data
        // To output
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            workbook.write(stream);
            stream.flush();
            byte[] buffer = stream.toByteArray();
            JsonObject headers = new JsonObject()
                .add("Access-Control-Allow-Origin", "*")
                .add("Content-Length", buffer.length)
                .add("Content-Type", "application/xlsx");
            // create response
            JsonObject response = new JsonObject()
                .add("isBase64Encoded", true)
                .add("statusCode", 200)
                .add("headers", headers)
                .add("body", buffer);
            // create writer
            logger.info("Serializing report output");
            try (JsonWriter writer = new JsonWriter(output, false)) {
                // write response
                writer.write(response);
            }
            output.flush();
        }
        catch (IOException e) {
            logger.error("Error sending output to client.", e);
        }
    }

    void fillInvestmentsPages(String tenantId, String pid, String token, Workbook workbook, Map<String, Sheet> investmentSheets) {
        // Prepare styles
        CellStyle dateCellStyle = workbook.createCellStyle();
        CreationHelper createHelper = workbook.getCreationHelper();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("mm/dd/yyyy"));
        dateCellStyle.setAlignment(HorizontalAlignment.RIGHT);
        CellStyle percentStyle = workbook.createCellStyle();
        percentStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00%"));
        percentStyle.setAlignment(HorizontalAlignment.RIGHT);
        CellStyle textStyle = workbook.createCellStyle();
        textStyle.setAlignment(HorizontalAlignment.LEFT);
        CellStyle amountStyle = workbook.createCellStyle();
        amountStyle.setDataFormat(workbook.createDataFormat().getFormat("_($#,##0_);_($ (#,##0);_($ \"-\"??_);_(@_)"));
        amountStyle.setAlignment(HorizontalAlignment.RIGHT);

        String filter = (pid == null) ? "manager/investmentStrategy ne 'N/A' and period/businessEvent/status/value eq 'Running'" : "period/id eq '" + pid + "'";
        logger.info("Running query for GcmManagerPricingEntryLine with filter [{}]", filter);
        JsonArray investments = runQuery(token, tenantId, "GcmManagerPricingEntryLine",
            "id,manager/managerName as 'managerName',manager/investmentStrategy as 'investmentStrategy', monthToDateRateOfReturn,period/periodEndDate as 'periodEndDate', manager/investmentStrategy as 'strategy', manager/investmentSubStrategy as 'substrategy',openingBalance",
            filter, "manager/investmentStrategy,manager/managerName", false);
        investments.getValuesAs(JsonObject.class).forEach(investment -> {
            String strategy = investment.getString("strategy");
            // Create a sheet name that excludes unsupported characters. Use that as both key and sheet name
            final String strategySheetName = cleanStrategyName(strategy);
            if (!investmentSheets.containsKey(strategySheetName)) {
                logger.info("Creating sheet [{}] for Strategy [{}]", strategySheetName, strategy);
                Sheet newSheet = workbook.createSheet(strategySheetName);
                logger.info("Total number of sheets after creation of [{}]/[{}] ({})", strategySheetName, strategy, workbook.getNumberOfSheets());
                investmentSheets.put(strategySheetName, newSheet);
                Row titles = newSheet.createRow(0);
                addTitles(titles, "Investment", "MTD ROR (%)", "Strategy", "Allocation ($)");
                Cell cell = titles.getCell(0);
                CellStyle style = titles.getSheet().getWorkbook().createCellStyle();
                style.setAlignment(HorizontalAlignment.LEFT);
                style.setVerticalAlignment(VerticalAlignment.CENTER);
                style.setWrapText(true);
                XSSFFont font = ((XSSFWorkbook)workbook).createFont();
                font.setBold(true);
                style.setFont(font);
                cell.setCellStyle(style);
                cell = titles.getCell(2);
                cell.setCellStyle(style);
                style = titles.getSheet().getWorkbook().createCellStyle();
                style.setAlignment(HorizontalAlignment.RIGHT);
                style.setVerticalAlignment(VerticalAlignment.CENTER);
                style.setWrapText(true);
                style.setFont(font);
                cell = titles.getCell(1);
                cell.setCellStyle(style);
                cell = titles.getCell(3);
                cell.setCellStyle(style);
            }
            Sheet sheet = investmentSheets.get(strategySheetName);
            Row row = sheet.createRow(sheet.getLastRowNum() + 1);
            // Add a cell to it
            Cell cell = row.createCell(0);
            sheet.setColumnWidth(0, 10000);
            cell.setCellValue(investment.getString("managerName"));
            cell = row.createCell(1);
            sheet.setColumnWidth(1, 3000);
            if (investment.get("monthToDateRateOfReturn") != null && investment.get("monthToDateRateOfReturn") != JsonValue.NULL) {
                cell.setCellValue(investment.getJsonNumber("monthToDateRateOfReturn").doubleValue());
                cell.setCellStyle(percentStyle);
            }
            else {
                cell.setCellStyle(percentStyle);
            }
            // Strategy
            cell = row.createCell(2);
            sheet.setColumnWidth(2, 11000);
            cell.setCellStyle(textStyle);
            cell.setCellValue(investment.getString("strategy") + " : " + investment.getString("substrategy"));
            // Amount
            cell = row.createCell(3);
            cell.setCellStyle(amountStyle);
            sheet.setColumnWidth(3, 8000);
            if (investment.get("openingBalance") != null && investment.get("openingBalance").getValueType() != JsonValue.ValueType.NULL)
                cell.setCellValue(investment.getJsonNumber("openingBalance").doubleValue());
        });
        investmentSheets.forEach((key, sheet) -> {
            // Handle heat map
            logger.info("Creating heat map for sheet [{}]", key);
            SheetConditionalFormatting layer = sheet.getSheetConditionalFormatting();
            ConditionalFormattingRule rule = layer.createConditionalFormattingColorScaleRule();
            ColorScaleFormatting pattern = rule.getColorScaleFormatting();
            pattern.getThresholds()[0].setRangeType(ConditionalFormattingThreshold.RangeType.MIN);
            pattern.getThresholds()[1].setRangeType(ConditionalFormattingThreshold.RangeType.PERCENTILE);
            pattern.getThresholds()[1].setValue((double)50);
            pattern.getThresholds()[2].setRangeType(ConditionalFormattingThreshold.RangeType.MAX);
            XSSFColor[] colors = new XSSFColor[]{new XSSFColor(), new XSSFColor(), new XSSFColor()};
            colors[0].setARGBHex("F8696B");
            colors[1].setARGBHex("FFEB84");
            colors[2].setARGBHex("63BE7B");
            pattern.setColors(colors);
            CellRangeAddress[] range1 = {CellRangeAddress.valueOf("B2:B200")};
            layer.addConditionalFormatting(range1, rule);
            // Freeze title
            sheet.createFreezePane(0, 1);
        });
    }

    private String cleanStrategyName(String sheetName) {
        StringBuilder builder = new StringBuilder();
        int len = sheetName.length();
        int idx = 0;
        while (idx < len) {
            char ch = sheetName.charAt(idx);
            switch (ch) {
                case '*':
                case '/':
                case ':':
                case '?':
                case '[':
                case '\\':
                case ']':
                case '\'':
                    ch = ' ';   // Replace invalid characters with spaces
                    ++idx;
                    break;
                default:
                    ++idx;
                    break;
            }
            builder.append(ch);
        }
        return builder.toString();
    }

    void fillFundPage(String tenantId, String pid, String token, Workbook workbook) {
        Sheet sheet = workbook.getSheet(GROSVENOR_FUND);
        // Add headers
        Row row = sheet.createRow(0);
        addTitles(row, "Effective Date", "Portfolio", "Currency", "Gross MTD ROR (%)", "Fiscal YTD ROR (%)", "Percent Priced (%)");
        // Left align headers that be
        Cell cell = row.getCell(0);
        CellStyle style = row.getSheet().getWorkbook().createCellStyle();
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        XSSFFont font = ((XSSFWorkbook)workbook).createFont();
        font.setBold(true);
        style.setFont(font);
        style.setWrapText(false);
        cell.setCellStyle(style);
        cell = row.getCell(1);
        cell.setCellStyle(style);
        cell = row.getCell(2);
        cell.setCellStyle(style);
        style = row.getSheet().getWorkbook().createCellStyle();
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setWrapText(false);
        style.setFont(font);
        cell = row.getCell(3);
        cell.setCellStyle(style);
        cell = row.getCell(4);
        cell.setCellStyle(style);
        cell = row.getCell(5);
        cell.setCellStyle(style);
        // Freeze first row
        sheet.createFreezePane(0, 1);
        // Get data from data source
        String filter = (pid == null) ? "header/businessEvent/status/value eq 'Running'" : "periodEndDate eq " + getPeriodEndDate(tenantId, pid, token);
        JsonArray data = runQuery(token, tenantId, "GcmInternalRankingReportLine",
            "portfolio/id as 'pid', periodEndDate as 'effectiveDate', portfolio/portfolioAcronym as 'grosvenorFund', portfolio/portfolioCurrency as 'currency', monthToDateRateOfReturn, ytdRateOfReturn, monthToDatePercentReporting as 'percentFinals', monthToDateCtrReporting as 'contributionToReturn'",
            filter, "portfolio/portfolioAcronym", true);
        // Fill rows
        for (int i = 0; i < data.size(); i++) {
            JsonObject dataRow = data.getJsonObject(i);
            row = sheet.createRow(i + 1);
            populateFundRow(row, dataRow);
        }
        // Handle heat map
        SheetConditionalFormatting layer = sheet.getSheetConditionalFormatting();
        ConditionalFormattingRule rule = layer.createConditionalFormattingColorScaleRule();
        ColorScaleFormatting pattern = rule.getColorScaleFormatting();
        pattern.getThresholds()[0].setRangeType(ConditionalFormattingThreshold.RangeType.MIN);
        pattern.getThresholds()[1].setRangeType(ConditionalFormattingThreshold.RangeType.PERCENTILE);
        pattern.getThresholds()[1].setValue((double)50);
        pattern.getThresholds()[2].setRangeType(ConditionalFormattingThreshold.RangeType.MAX);
        XSSFColor[] colors = new XSSFColor[]{new XSSFColor(), new XSSFColor(), new XSSFColor()};
        colors[0].setARGBHex("F8696B");
        colors[1].setARGBHex("FFEB84");
        colors[2].setARGBHex("63BE7B");
        pattern.setColors(colors);
        CellRangeAddress[] range1 = {CellRangeAddress.valueOf("D2:D200")};
        layer.addConditionalFormatting(range1, rule);
        CellRangeAddress[] range2 = {CellRangeAddress.valueOf("E2:E200")};
        layer.addConditionalFormatting(range2, rule);
        CellRangeAddress[] range3 = {CellRangeAddress.valueOf("F2:F200")};
        layer.addConditionalFormatting(range3, rule);
    }

    private String getPeriodEndDate(String tenantId, String pid, String token) {
        JsonArray results = runQuery(token, tenantId, "GcmManagerPricingData", "periodEndDate", "id eq '" + pid + "'", null, false);
        if (results.size() > 0)
            return results.getJsonObject(0).getString("periodEndDate");
        return "1900-01-01";
    }

    private void populateFundRow(Row row, JsonObject dataRow) {
        Workbook workbook = row.getSheet().getWorkbook();
        Sheet sheet = row.getSheet();
        CellStyle dateCellStyle = workbook.createCellStyle();
        CreationHelper createHelper = workbook.getCreationHelper();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("mm/dd/yyyy"));
        dateCellStyle.setAlignment(HorizontalAlignment.LEFT);
        // Date
        Cell cell = row.createCell(0);
        sheet.setColumnWidth(0, 4000);
        cell.setCellStyle(dateCellStyle);
        try {
            cell.setCellValue((new SimpleDateFormat("yyyy-MM-dd")).parse(dataRow.getString("effectiveDate")));
        }
        catch (ParseException e) {
            logger.error("Could parse date {} from data source", dataRow.getString("effectiveDate"));
        }
        // Fund
        row.createCell(1).setCellValue(dataRow.getString("grosvenorFund"));
        sheet.setColumnWidth(1, 9000);
        // Currency
        cell = row.createCell(2);
        cell.setCellValue(dataRow.getString("currency", "USD"));
        CellStyle textStyle = workbook.createCellStyle();
        textStyle.setAlignment(HorizontalAlignment.LEFT);
        cell.setCellStyle(textStyle);
        sheet.setColumnWidth(2, 2000);
        // MTD ROR
        CellStyle percentStyle = workbook.createCellStyle();
        percentStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00%"));
        percentStyle.setAlignment(HorizontalAlignment.RIGHT);
        CellStyle intPercentStyle = workbook.createCellStyle();
        intPercentStyle.setDataFormat(workbook.createDataFormat().getFormat("0%"));
        intPercentStyle.setAlignment(HorizontalAlignment.RIGHT);
        cell = row.createCell(3);
        cell.setCellStyle(percentStyle);
        sheet.setColumnWidth(3, 5000);
        if (dataRow.get("monthToDateRateOfReturn") != null && dataRow.get("monthToDateRateOfReturn").getValueType() != JsonValue.ValueType.NULL)
            cell.setCellValue(dataRow.getJsonNumber("monthToDateRateOfReturn").doubleValue());
        // YTD ROR
        cell = row.createCell(4);
        sheet.setColumnWidth(4, 4000);
        cell.setCellStyle(percentStyle);
        if (dataRow.get("ytdRateOfReturn") != null && dataRow.get("ytdRateOfReturn").getValueType() != JsonValue.ValueType.NULL)
            cell.setCellValue(dataRow.getJsonNumber("ytdRateOfReturn").doubleValue());
        // Percent Finals
        cell = row.createCell(5);
        sheet.setColumnWidth(5, 4000);
        cell.setCellStyle(percentStyle);
        if (dataRow.get("percentFinals") != null && dataRow.get("percentFinals").getValueType() != JsonValue.ValueType.NULL)
            cell.setCellValue(dataRow.getJsonNumber("percentFinals").doubleValue());
    }

    private void addTitles(Row row, String... headers) {
        CellStyle style = row.getSheet().getWorkbook().createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setWrapText(true);
        for (int i = 0; i < headers.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellStyle(style);
            cell.setCellValue(headers[i]);
        }
    }
}
