package com.exchangelodge.gcm.reporting.lambda.services;

import com.exchangelodge.gcm.reporting.lambda.LambdaFunctionException;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.RpcClient;
import com.steelbridgelabs.cbe.json.JsonObject;
import com.steelbridgelabs.cbe.json.JsonReader;
import com.steelbridgelabs.cbe.json.JsonString;
import com.steelbridgelabs.cbe.json.JsonWriter;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

public class MessagingService {

    private static final String entityGetRoutingKey = ".entity.entity.get";

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MessagingService.class);
    private final String exchangeName;
    private AtomicReference<ConnectionFactory> connectionFactory = new AtomicReference<>();
    private final String entityRoutingKeyPrefix;

    public MessagingService(String serverAddress, int serverPort, String virtualHost, String userName, String password, String exchange, String entityRoutingKeyPrefix) {
        initializeConnectionFactory(serverAddress, serverPort, virtualHost, userName, password);
        // Set exchange
        this.exchangeName = exchange;
        this.entityRoutingKeyPrefix = entityRoutingKeyPrefix == null ? "entity.$cbe-version." : entityRoutingKeyPrefix;
    }

    public String getEntityRoutingKey() {
        return entityRoutingKeyPrefix + entityGetRoutingKey;
    }

    @SuppressFBWarnings(value = {"RCN_REDUNDANT_NULLCHECK_WOULD_HAVE_BEEN_A_NPE"}, justification = "This is a reported bug in spotbugs. Unsolved as of 2021-10-12")
    public JsonObject sendAndReceive(JsonObject message, String routingKey, JsonObject headers) {
        ConnectionFactory factory = getConnectionFactory();
        try (Connection connection = factory.newConnection()) {
            Channel channel = connection.createChannel();
            RpcClient client = new RpcClient(channel, exchangeName, routingKey);
            // Prepare message, send and receive
            // log information
            logger.debug("Creating message with Json Value: {} headers: {} on routing key: {}", message, headers, routingKey);
            // extract known fields and set them as header values
            Map<String, Object> messageHeaders = new HashMap<>();
            headers.forEach((key, value) -> messageHeaders.put(key, ((JsonString)value).getString()));
            try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
                // create writer
                try (OutputStreamWriter writer = new OutputStreamWriter(stream, Charset.forName("UTF-8"))) {
                    // create json writer
                    try (JsonWriter jsonWriter = new JsonWriter(writer)) {
                        // write json to stream
                        jsonWriter.write(message);
                    }
                }
                catch (IOException ex) {
                    // throw exception
                    logger.error("Error serializing message", ex);
                    throw new LambdaFunctionException("Error serializing to JSON", 500, ex);
                }
                // read stream buffer
                byte[] buffer = stream.toByteArray();
                messageHeaders.put("Content-Length", buffer.length);
                AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder().contentType("application/cbe-json").headers(messageHeaders).build();
                logger.info("Calling Rabbit RPC client");
                RpcClient.Response response = client.doCall(properties, buffer);
                logger.info("Response received from client.");
                try (ByteArrayInputStream inputStream = new ByteArrayInputStream(response.getBody())) {
                    try (InputStreamReader inputReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"))) {
                        try (JsonReader reader = new JsonReader(inputReader)) {
                            return reader.readObject();
                        }
                    }
                }
            }
        }
        catch (IOException e) {
            // throw exception
            logger.error("Error serializing message", e);
            throw new LambdaFunctionException("Error serializing to JSON", 500, e);
        }
        catch (TimeoutException e) {
            logger.error("Timeout sending message to consumer", e);
            throw new LambdaFunctionException(e.getMessage(), 504, e);
        }
    }

    void initializeConnectionFactory(String serverAddress, int serverPort, String virtualHost, String userName, String password) {
        if (getConnectionFactory() == null) {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername(userName);
            factory.setPassword(password);
            factory.setHost(serverAddress);
            factory.setPort(serverPort);
            factory.setVirtualHost(virtualHost);
            connectionFactory.getAndSet(factory);
        }
    }

    ConnectionFactory getConnectionFactory() {
        return connectionFactory.get();
    }
}
