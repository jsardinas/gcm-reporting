package com.exchangelodge.gcm.reporting.lambda.services;

import com.steelbridgelabs.cbe.json.Json;
import com.steelbridgelabs.cbe.json.JsonArray;
import com.steelbridgelabs.cbe.json.JsonNumber;
import com.steelbridgelabs.cbe.json.JsonObject;
import com.steelbridgelabs.cbe.json.JsonObjectBuilder;
import com.steelbridgelabs.cbe.json.JsonString;
import com.steelbridgelabs.cbe.json.JsonValue;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRField;

public class ClassesDataSource implements JRDataSource {

    private final MessagingService messagingService;
    private final String filter;
    private static final String entityName = "GcmPortfolioInvestmentDetailClass";
    private static final String select = "portfolioShareClass/shareClassName as 'concept',balanceForward,beginningNetActivity,openingBalance,income as 'netGainLoss',endingBalance,endingNetActivity,closingBalance,rateOfReturn as 'mtdRateOfReturn',ytdRateOfReturn,contributionToReturn,ytdContributionToReturn,"
        + "portfolioInfo/shareClassSummary/balanceForward as 'totalBalanceForward',portfolioInfo/shareClassSummary/beginningNetActivity as 'totalBeginningNetActivity',portfolioInfo/shareClassSummary/openingBalance as 'totalOpeningBalance',"
        + "portfolioInfo/shareClassSummary/netGainLoss as 'totalNetGainLoss',portfolioInfo/shareClassSummary/endingBalance as 'totalEndingBalance',portfolioInfo/shareClassSummary/endingNetActivity as 'totalEndingNetActivity',portfolioInfo/shareClassSummary/closingBalance as 'totalClosingBalance',"
        + "portfolioInfo/shareClassSummary/contributionToReturn as 'totalMtdContributionToReturn',portfolioInfo/shareClassSummary/contributionToReturnYtd as 'totalYtdContributionToReturn',portfolioInfo/shareClassSummary/rateOfReturn as 'totalMtdRateOfReturn',portfolioInfo/shareClassSummary/rateOfReturnYtd as 'totalYtdRateOfReturn'";
    private final String tenantId;
    private final String authenticationToken;
    private JsonArray data = null;
    private int index;

    public ClassesDataSource(MessagingService messagingService, String authenticationToken, String filter, String tenantId) {
        this.authenticationToken = authenticationToken;
        this.messagingService = messagingService;
        this.filter = filter;
        this.tenantId = tenantId;
        this.index = -1;
    }

    @Override
    public boolean next() {
        if (data == null) {
            // Run the query. Reset index.
            data = runQuery();
            if (data != null && !data.isEmpty()) {
                index = 0;
                return true;
            }
            return false;
        }
        return (++index < data.size());
    }

    @Override
    public Object getFieldValue(JRField jrField) {
        JsonObject current = data.getJsonObject(index);
        if (current.containsKey(jrField.getName())) {
            JsonValue value = current.get(jrField.getName());
            switch (value.getValueType()) {
                case STRING:
                    return ((JsonString)value).getString();
                case NUMBER:
                    return ((JsonNumber)value).getDouble();
                case TRUE:
                case FALSE:
                    return current.getBoolean(jrField.getName());
                default:
                    return null;
            }
        }
        // Not present
        return null;
    }

    JsonArray runQuery() {
        String query = "select=" + select;
        if (filter != null)
            query += ("&filter=" + filter);
        query += "&orderby=name";
        query += "&top=1500";
        JsonObjectBuilder builder = Json.createObjectBuilder().add("parameters", Json.createArrayBuilder().add(entityName)).add("query", query);
        if (tenantId != null)
            builder.add("tenant", tenantId);
        JsonObject message = builder.build();
        builder = Json.createObjectBuilder().add("Content-Type", "application/cbe-json").add("x-cbe-authorization", authenticationToken);
        if (tenantId != null)
            builder.add("x-tenant-id", tenantId);
        JsonObject header = builder.build();
        JsonObject result = messagingService.sendAndReceive(message, messagingService.getEntityRoutingKey(), header);
        return result.getJsonArray("value");
    }
}
