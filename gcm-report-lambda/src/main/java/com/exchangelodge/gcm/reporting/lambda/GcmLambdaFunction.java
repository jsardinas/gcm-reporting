package com.exchangelodge.gcm.reporting.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.exchangelodge.gcm.reporting.lambda.services.ReportExecutionService;
import com.steelbridgelabs.cbe.aws.lambda.LambdaFunction;
import com.steelbridgelabs.cbe.aws.lambda.services.ConfigurationProvider;
import com.steelbridgelabs.cbe.aws.lambda.services.EnvironmentConfigurationProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public class GcmLambdaFunction extends LambdaFunction {

    private static final Logger logger = LoggerFactory.getLogger(GcmLambdaFunction.class);

    private final AtomicReference<ReportExecutionService> reportExecutionService = new AtomicReference<>();

    public GcmLambdaFunction(ConfigurationProvider configurationProvider) {
        super(configurationProvider);
    }

    public GcmLambdaFunction() {
        super(new EnvironmentConfigurationProvider());
    }

    @Override
    public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
        Objects.requireNonNull(input, "input cannot be null");
        Objects.requireNonNull(output, "output cannot be null");
        Objects.requireNonNull(context, "context cannot be null");
        try {
            logger.info("Request received with id [{}]", context.getAwsRequestId());
            handleRequest(input, output, context, (requestContext) -> {
                // Normal implementation
                // initialize report execution service
                ReportExecutionService reportExecutionService = initializeReportExecutionService();
                // execute report
                reportExecutionService.execute(requestContext.getEvent(), requestContext.getIdentity().getJwt(), output);
            });
            logger.info("Response returned for request [{}]", context.getAwsRequestId());
        }
        catch (LambdaFunctionException ex) {
            // write response
            logger.error("Execution error for request {} ", context.getAwsRequestId(), ex);
            writeErrorResponse(output, ex.getCode(), ex.getMessage());
        }
    }

    ReportExecutionService initializeReportExecutionService() {
        // check service has been initialized
        ReportExecutionService service = reportExecutionService.get();
        if (service == null) {
            // initialize it
            service = new ReportExecutionService();
            // update reference
            reportExecutionService.set(service);
        }
        return service;
    }
}
