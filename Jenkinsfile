#!groovy

node {

    // build properties.
    properties([
        // limit 1 build/branch
        disableConcurrentBuilds(),
        // clean up old builds
        [$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '7', artifactNumToKeepStr: '5', daysToKeepStr: '7', numToKeepStr: '5']]
    ])
    // initialize node
    initialize()
    // git commit hash
    def hash = sh (script: "git log -n 1 --pretty=format:'%H'", returnStdout: true)
    // notify BitBucket
    bitbucketStatus 'exchangelodge', 'gcm-reporting', hash, 'INPROGRESS'
    try {
        // do not build on master branch
        if (env.BRANCH_NAME != "master") {
            // build
            build()
            // publish results
            publishResults()
        }
        // notify build succeeded
        bitbucketStatus 'exchangelodge', 'gcm-reporting', hash, 'SUCCESSFUL'
    }
    catch (Exception ex) {
        // notify build failed
        bitbucketStatus 'exchangelodge', 'gcm-reporting', hash, 'FAILED'
        // throw exception
        throw ex
    }
}

def initialize() {
    // initialize
    stage('Initialize') {
        // mvn tool as configured in Jenkins
        def mvnHome = tool 'MVN'
        // java tool as configured in Jenkins
        def javaHome = tool 'JDK8'
        // update environment (PATH)
        env.PATH = "${env.PATH}:${mvnHome}/bin"
        // set JAVA_HOME
        env.JAVA_HOME = "${javaHome}"
        // checkout current branch
        checkout scm
    }
}

def bitbucketStatus(owner, repository, hash, state) {
    // use OAUTH credentials
    withCredentials([usernamePassword(credentialsId: 'Bitbucket-OAUTH', passwordVariable: 'OAUTH_PASSWORD', usernameVariable: 'OAUTH_USERNAME')]) {
        // json payload
        def data = "{ \"state\": \"${state}\", \"key\": \"${env.BUILD_ID}\", \"url\": \"${env.BUILD_URL}\", \"name\": \"${env.JOB_NAME}\" }";
        // api url
        def apiUrl = "https://api.bitbucket.org/2.0/repositories/${owner}/${repository}/commit/${hash}/statuses/build"
        // update build status (https://confluence.atlassian.com/bitbucket/integrate-your-build-system-with-bitbucket-cloud-790790968.html)
        sh "curl -u ${env.OAUTH_USERNAME}:${env.OAUTH_PASSWORD} -H 'Content-Type: application/json' -X POST ${apiUrl} --data '${data}'"
    }
}

def build() {
    // build
    stage('Build Code') {
        // maven settings file
        configFileProvider([configFile(fileId: 'maven-settings', variable: 'MAVEN_SETTINGS')]) {
            // build project
            sh "mvn -B -Dmaven.test.failure.ignore clean install -s ${env.MAVEN_SETTINGS} -f src/pom.xml"
        }
    }
}

def publishResults() {
    // checkstyle
    step([$class: 'CheckStylePublisher', canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''])
    // findbugs, FindBugsPublisher conflict with AWSCodePipeline plugin
    // step([$class: 'FindBugsPublisher', canComputeNew: false, defaultEncoding: '', excludePattern: '', healthy: '', includePattern: '', pattern: '', unHealthy: ''])
    // pmd
    step([$class: 'PmdPublisher', canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''])
    // jacoco
    step([$class: 'JacocoPublisher'])
}
